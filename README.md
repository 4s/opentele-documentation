# OpenTele documentation readme #

This file describes the OpenTele documentation repository.

### What is found here ###

The repository contains working copies of the documentation for the OpenTele project.  Released .pdf versions of the files can be found in the downloads tab.

The majority of the Documentation is currently in Danish, but it is highly recommended that any new sections or documents be written in English.